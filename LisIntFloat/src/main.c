#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "lista.h"
#include "lista.c"

int main(){
    //IMPLEMENTACIONES DE FUNCIONES
    //creación de la lista para valores int
    nodo *lista;
    lista=crearLista(lista);
    //inserción de datos en varias posiciones de la lista int
    lista=insertarNodoFIn(5, lista);
    lista=insertarNodoFIn(10, lista);
    lista=insertarNodoInicio(9, lista);
    lista=insertarNodoInicio(7,lista);

    imprimirlista(lista);

    lista=retirarInicio(lista);
    imprimirlista(lista);

    lista=retinarFin(lista);
    imprimirlista(lista);
 //////////////////////////////////////FLOAT/////////////
    //creacion de lista para valores float
    nodof *lista1;
    lista1=crearLista1(lista1);
    //INsercición de valores float a la lista
    lista1=insertarNodoFIn1(5.5, lista1);
    lista1=insertarNodoFIn1(1.2, lista1);
    lista1=insertarNodoInicio1(4.5, lista1);
    lista1=insertarNodoInicio1(6.7,lista1);
  
    printf("\n");
    imprimirlista1(lista1);

    //ELiminacion del primer valor de la lista
    lista1=retirarInicio1(lista1);
    imprimirlista1(lista1);

    //Eliminación del valor final de la lista
    lista1=retinarFin1(lista1);
    imprimirlista1(lista1);

    return 0;
}
