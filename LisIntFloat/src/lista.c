#include "lista.h"
#include <stdio.h>
#include <stdlib.h>


//Implementaciones de Funciones para INT

nodo *crearLista(nodo *lista){
    return lista = NULL;
}

nodo *insertarNodoInicio(int valor, nodo *lista){

    nodo *nodoNuevo;
    nodoNuevo = (nodo *) malloc(sizeof(nodo)); //mediante mallo se reserva un espacio de memoria
    if(nodoNuevo!=NULL){
        nodoNuevo->dato = valor; //SE PROCEDE INSERTAR EL NODO NUEVO A LA LISTA 
        nodoNuevo->siguiente=lista;

        //decirle a lista que apunte al noodo nuevo
        lista=nodoNuevo;
    }

    //se regresa lista actualizada
    return lista;
}

nodo *insertarNodoFIn(int valor, nodo *lista){
    //declarar el nodo nuevo
    nodo *nodoNuevo, *nodoAux;
    //reservar espacio en la memoria
    nodoNuevo = (nodo *) malloc(sizeof(nodo));

    //verificar que hay espacio ya reservado en memoria
    if(nodoNuevo!=NULL){
        nodoNuevo->dato=valor;
        //ESte nodo para a ser el último elemento de la lista
        nodoNuevo->siguiente=NULL;

        //comprobar si la lista esta vacia o no
        if(lista==NULL)
            lista=nodoNuevo; //que la liste apunte al nodo nuevo
        
        else{

            nodoAux=lista;
            //hacer que el nodo Auxiliar salte de nodo en nodo hasta el fin de la lista
            while(nodoAux->siguiente!= NULL)
                nodoAux=nodoAux->siguiente;
            //al terminar el ciclo hacer que nodo AUX apunte al nodo nuevo
            nodoAux->siguiente=nodoNuevo;
        }
        
    }
    //retornar la lista ya actualizada
    return lista;
}


//función que imprime la lista
//se debe hacer todo el recorrido de la lista y declarar un nod auxiliar
void imprimirlista(nodo *lista){
    nodo *nodoAux;
    nodoAux=lista;

    printf("Lista int: ");

    //RECORRER CON WHILE TODOS LOS CAMPOS DE LISTA
    while(nodoAux!=NULL){
        //imprimir el campo de datos de cada nodo
        printf("%d -> ", nodoAux->dato);
        //nodo auxiliar apunte al siguiente nodo
        nodoAux=nodoAux->siguiente;
    }
    printf("\n");

}

nodo *retirarInicio(nodo *lista){
    //
    nodo *nodoAux;
    int dato;
    //verificar que la lista no este vacia
    if(lista==NULL)
        printf("No hay elementos en lista ");
    else{
        nodoAux=lista;
        dato=nodoAux->dato; //se asegura que se tiene el dato almacenado antes de retirar de la lista

        lista=nodoAux->siguiente; 

        printf("\nSe elimino el elemento de inicio: %d\n ", dato);
        //Se asegura que el elemento ya no esta en lista pero el espacio de memoria sigue ocupado
        //Liberar espacio de memoria
        free(nodoAux); //COn free se libera el espacio y asi estar disponible para un nuevo nodo

    }
    return lista;
}

nodo *retinarFin(nodo *lista){
    //la lista solo puede ser accesada desde el inicio se debe hacer un recorrido y se ocupan dos nodos auxiliares
    //nodoAux se encargara de hacer todo el recorrido de la lista
    //nodoAntes guardara un nodo antes del último nodo de la lista
    nodo *nodoAux, *nodoAntes;
    //se necesita una variable entera que guarde el dato a retirar
    int dato;

    //comprobar que la lista no este vacia
    if(lista==NULL)
        printf("No hay elementos en lista");
    else{
        nodoAux=lista;
        while(nodoAux->siguiente!=NULL){
            nodoAntes=nodoAux;
            nodoAux=nodoAux->siguiente;
        }
        dato=nodoAux->dato; 

        nodoAntes->siguiente=NULL; //El nodo que apunte a null es el ultimo de la lista

        printf("\nSe elimino el elemento del final: %d\n", dato);

        free(nodoAux);
    }

    return lista;
}
//////////////////////////////
/////////////////////////////
/////////////////////////////
/////////////////LISTA FLOAT/////////
nodof *crearLista1(nodof *lista1){
    return lista1 = NULL;
}

nodof *insertarNodoInicio1(float valor1, nodof *lista1){

    nodof *nodoNuevo;
    nodoNuevo = (nodof *) malloc(sizeof(nodof)); //mediante mallo se reserva un espacio de memoria
    if(nodoNuevo!=NULL){
        nodoNuevo->dato1 = valor1; //SE PROCEDE INSERTAR EL NODO NUEVO A LA LISTA 
        nodoNuevo->siguiente1=lista1;

        //decirle a lista que apunte al noodo nuevo
        lista1=nodoNuevo;
    }

    //se regresa lista actualizada
    return lista1;
}

nodof *insertarNodoFIn1(float valor1, nodof *lista1){
    //declarar el nodo nuevo
    nodof *nodoNuevo, *nodoAux;
    //reservar espacio en la memoria
    nodoNuevo = (nodof *) malloc(sizeof(nodof));

    //verificar que hay espacio ya reservado en memoria
    if(nodoNuevo!=NULL){
        nodoNuevo->dato1=valor1;
        //ESte nodo para a ser el último elemento de la lista
        nodoNuevo->siguiente1=NULL;

        //comprobar si la lista esta vacia o no
        if(lista1==NULL)
            lista1=nodoNuevo; //que la liste apunte al nodo nuevo
        
        else{

            nodoAux=lista1;
            //hacer que el nodo Auxiliar salte de nodo en nodo hasta el fin de la lista
            while(nodoAux->siguiente1!= NULL)
                nodoAux=nodoAux->siguiente1;
            //al terminar el ciclo hacer que nodo AUX apunte al nodo nuevo
            nodoAux->siguiente1=nodoNuevo;
        }
        
    }
    //retornar la lista ya actualizada
    return lista1;
}


//función que imprime la lista
//se debe hacer todo el recorrido de la lista y declarar un nod auxiliar
void imprimirlista1(nodof *lista1){
    nodof *nodoAux;
    nodoAux=lista1;

    printf("Lista float: ");

    //RECORRER CON WHILE TODOS LOS CAMPOS DE LISTA
    while(nodoAux!=NULL){
        //imprimir el campo de datos de cada nodo
        printf("%.2f -> ", nodoAux->dato1);
        //nodo auxiliar apunte al siguiente nodo
        nodoAux=nodoAux->siguiente1;
    }
    printf("\n");

}


nodof *retirarInicio1(nodof *lista1){
    //
    nodof *nodoAux;
    float dato;
    //verificar que la lista no este vacia
    if(lista1==NULL)
        printf("No hay elementos en lista ");
    else{
        nodoAux=lista1;
        dato=nodoAux->dato1; //se asegura que se tiene el dato almacenado antes de retirar de la lista

        lista1=nodoAux->siguiente1; 

        printf("\nSe elimino el elemento de inicio: %.2f\n ", dato);
        //Se asegura que el elemento ya no esta en lista pero el espacio de memoria sigue ocupado
        //Liberar espacio de memoria
        free(nodoAux); //COn free se libera el espacio y asi estar disponible para un nuevo nodo

    }
    return lista1;
}

nodof *retinarFin1(nodof *lista1){
    //la lista solo puede ser accesada desde el inicio se debe hacer un recorrido y se ocupan dos nodos auxiliares
    //nodoAux se encargara de hacer todo el recorrido de la lista
    //nodoAntes guardara un nodo antes del último nodo de la lista
    nodof *nodoAux, *nodoAntes;
    //se necesita una variable entera que guarde el dato a retirar
    float dato;

    //comprobar que la lista no este vacia
    if(lista1==NULL)
        printf("No hay elementos en lista");
    else{
        nodoAux=lista1;
        while(nodoAux->siguiente1!=NULL){
            nodoAntes=nodoAux;
            nodoAux=nodoAux->siguiente1;
        }
        dato=nodoAux->dato1; 

        nodoAntes->siguiente1=NULL; //El nodo que apunte a null es el ultimo de la lista

        printf("\nSe elimino el elemento del final: %.2f\n", dato);

        free(nodoAux);
    }

    return lista1;
}