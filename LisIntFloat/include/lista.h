/**
 * @file    lista.h
 * @brief   Cabecera del fichero lista.c
 * @par		Declaraciones 
 *			- structNodo Definicion de la estructura que almacenara los nodos int.
 *			- *crearLista Definicion de la estructura para la creación de la lista int.
 *			- *insertarNodoInicio Definicion de la estructura para la inserción al inicio de la lista int.
 *			- *insertarNodoFIn Definicion de la estructura para la inserción al final de la lista int.
 *          - *retirarInicio Definicion de la estructura para la eliminacion al inicio de un elemento de la lista int.
 *          - *retirarFin Definicion de la estructura para la eliminacion al final de un elemento de la lista int.
 *          - imprimirlista Definicion de la estructura para mostrar los datos o elementos de la lista int.
 * 
 * 			- structNodof Definicion de la estructura que almacenara los nodos float.
 *			- *crearLista1 Definicion de la estructura para la creación de la lista float.
 *			- *insertarNodoInicio1 Definicion de la estructura para la inserción al inicio de la lista float.
 *			- *insertarNodoFIn1 Definicion de la estructura para la inserción al final de la lista float.
 *          - *retirarInicio1 Definicion de la estructura para la eliminacion al inicio de un elemento de la lista float.
 *          - *retirarFin1 Definicion de la estructura para la eliminacion al final de un elemento de la lista float.
 *          - imprimirlista1 Definicion de la estructura para mostrar los datos o elementos de la lista float.
 * @author  J. Manuel Octaviano
 * @date    17/04/2020 
 */

#ifndef LISTA_H
#define LISTA_H

//LISTA INT

struct structNodo{
    /**
 	* @brief   dato --> define el campo que es para los datos
 	*/
    int dato;
    /**
 	* @brief   *siguiente --> define o hace referencia al siguiente nodo 
 	*/
    struct structNodo *siguiente;
};

//definicion del tipo de dato nodo
typedef struct structNodo nodo; 

/**
 	* @brief   *crearLista --> Definicion de la estructura para la creación de la lista int.
 	* @param   *lista es un apuntador hacia una lista
 	* @return  un nodo 
 	*/
nodo *crearLista(nodo *lista);

/**
 	* @brief   *insertarNodoInicio --> Definicion de la estructura para la inserción al inicio de la lista int.
 	* @param   valor se recibe un valor entero
    * @param   *lista se recibe un apuntador al inicio de la lista
 	* @return  un nodo agregado al inicio de la lista
 	*/
nodo *insertarNodoInicio(int valor, nodo *lista);

/**
 	* @brief   *insertarNodoFIn --> Definicion de la estructura para la inserción al final de la lista int.
 	* @param   valor se recibe un valor entero
    * @param   *lista se recibe un apuntador al final de la lista
 	* @return  un nodo agregado al final de la lista
 	*/
nodo *insertarNodoFIn(int valor, nodo *lista);

/**
 	* @brief   *retirarInicio --> Definicion de la estructura para la eliminacion al inicio de un elemento de la lista int.
    * @param   *lista se recibe una lista
 	
 	*/
nodo *retirarInicio(nodo *lista);

/**
 	* @brief   *retirarFin --> Definicion de la estructura para la eliminacion al final de un elemento de la lista int.
    * @param   *lista se recibe una lista
 	
 	*/
nodo *retinarFin(nodo *lista);

/**
 	* @brief   imprimirlista --> Definicion de la estructura para mostrar los datos o elementos de la lista int.
    * @param   *lista se recibe los datos a imprimir de la lista
 	* @return  void
 	*/
void imprimirlista(nodo *lista);

////////////////////////////////////////////////////////////
//LISTA FLOAT
////////////////////////////////////////////////////////
struct structNodof{
    /**
 	* @brief   dato1 --> define el campo que es para los datos
 	*/
    float dato1;
    /**
 	* @brief   *siguiente1 --> define o hace referencia al siguiente nodo 
 	*/
    struct structNodof *siguiente1;
};

typedef struct structNodof nodof;


/**
 	* @brief   *crearLista1 --> Definicion de la estructura para la creación de la lista float.
 	* @param   *lista1 es un apuntador hacia una lista
 	* @return  un nodof 
 	*/
nodof *crearLista1(nodof *lista1);

/**
 	* @brief   *insertarNodoInicio1 --> Definicion de la estructura para la inserción al inicio de la lista float.
 	* @param   valor1 se recibe un valor entero
    * @param   *lista1 se recibe un apuntador al inicio de la lista
 	* @return  un nodof agregado al inicio de la lista
 	*/
nodof *insertarNodoInicio1(float valor1, nodof *lista1);

/**
 	* @brief   *insertarNodoFIn1 --> Definicion de la estructura para la inserción al final de la lista float.
 	* @param   valor1 se recibe un valor entero
    * @param   *lista1 se recibe un apuntador al final de la lista
 	* @return  un nodof agregado al final de la lista
 	*/
nodof *insertarNodoFIn1(float valor1, nodof *lista1);

/**
 	* @brief   *retirarInicio1 --> Definicion de la estructura para la eliminacion al inicio de un elemento de la lista float.
    * @param   *lista1 se recibe una lista
 	*/
nodof *retirarInicio1(nodof *lista1);

/**
 	* @brief   *retirarFin1 --> Definicion de la estructura para la eliminacion al final de un elemento de la lista float.
    * @param   *lista1 se recibe una lista
 	*/
nodof *retinarFin1(nodof *lista1);

/**
 	* @brief   imprimirlista --> Definicion de la estructura para mostrar los datos o elementos de la lista float.
    * @param   *lista se recibe los valores almacenados en la lista
 	* @return  void
 	*/
void imprimirlista1(nodof *lista1);

#endif //LISTA_H